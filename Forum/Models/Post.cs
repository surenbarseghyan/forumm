﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
        public string  UserId { get; set; }
        public Topic Topic { get; set; }
        public int TopicId { get; set; }
        public ICollection<Comment> Comments { get; set; }

    }
}
