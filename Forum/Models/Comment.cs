﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Models
{
    public class Comment
    {
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        public int PostId { get; set; }

    }
}
