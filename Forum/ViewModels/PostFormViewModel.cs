﻿using Forum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.ViewModels
{
    public class PostFormViewModel
    {
        public string Title { get; set; }
        public Post Post { get; set; }
        public IEnumerable<Topic> Topics { get; set; }
        
    }
}
