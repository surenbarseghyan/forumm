﻿using Forum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.ViewModels
{
    public class SearchViewResult
    {
        public List<Post> Posts { get; internal set; }
    }
}
