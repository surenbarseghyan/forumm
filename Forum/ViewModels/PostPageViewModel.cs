﻿using Forum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.ViewModels
{
    public class PostPageViewModel
    {
        public Post Post { get; set; }
        public Comment Comment { get; set; }
        public List<Comment> Comments { get; internal set; }
    }
}
