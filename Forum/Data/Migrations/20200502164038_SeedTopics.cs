﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Forum.Data.Migrations
{
    public partial class SeedTopics : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Topics (Name) VALUES ('Programming')");
            migrationBuilder.Sql("INSERT INTO Topics (Name) VALUES ('Sport')");
            migrationBuilder.Sql("INSERT INTO Topics (Name) VALUES ('News')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Topic WHERE NAME IN('Programming','Sport','News')");
        }
    }
}
