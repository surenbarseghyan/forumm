﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.Data;
using Forum.Models;
using Forum.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Forum.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<User> userManager;

        public PostController(ApplicationDbContext context, UserManager<User> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }
        public IActionResult New()
        {
            PostFormViewModel postForm = new PostFormViewModel
            {
                Title = "New",
                Topics = context.Topics
            };
            return View(postForm);
        }
        public IActionResult Result(string q)
        {
            var query = context.Posts.
                Where(p => p.Title.Contains(q))
                .ToList();
            if (query == null)
            {
                return NotFound();
            }
            SearchViewResult result = new SearchViewResult
            {
                Posts = query
            };
            return View(result);
        }
        [Authorize]
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> New(Post post)
        {
            var user = await userManager.GetUserAsync(User);
            if (!ModelState.IsValid)
            {
                return BadRequest();
               
            }
            post.UserId = user.Id;
            context.Posts.Add(post);
            context.SaveChanges();
            return RedirectToAction("Index", "Account");

        }
        [HttpPost]
        [Authorize]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Index(int id,PostPageViewModel model)
        {
            var user = await userManager.GetUserAsync(User);
            model.Comment.PostId = id;
            model.Comment.UserId = user.Id;
            context.Comments.Add(model.Comment);
            context.SaveChanges();
            model.Comment = null;
            return RedirectToAction("Index",new {id =id });
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Index(int id)
        {
            string signedUserId = userManager.GetUserId(User);
            Post post = context.Posts.Where(p => p.Id == id).Include(p=>p.Topic).SingleOrDefault();
            var comments = context.Comments.Where(c => c.PostId == id).Include(u=>u.User).ToList();
            PostPageViewModel model = new PostPageViewModel
            {
                Post = post,
                Comments = comments
            };
            if(post.UserId == signedUserId)
            {
            return View("AdminPost",model);
                
            }
            return View(model);
        }
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await userManager.GetUserAsync(User);
            var post = context.Posts.Where(p => p.Id == id).SingleOrDefault();
            var topics = context.Topics.ToList();
            if (post == null)
            {
                return NotFound();

            }
            if (post.UserId != user.Id)
            {
                return BadRequest();
            }
            PostFormViewModel postForm = new PostFormViewModel
            {
                Title = "Edit",
                Post = post,
                Topics = topics
            };
            return View("New", postForm);
        }

        [HttpPost]
        [Authorize]
        [AutoValidateAntiforgeryToken]
        public IActionResult Edit([FromForm]PostFormViewModel post_new)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var post = context.Posts.Where(p => p.Id == post_new.Post.Id).SingleOrDefault();
            post.Title = post_new.Post.Title;
            post.Description = post_new.Post.Description;
            post.TopicId = post_new.Post.TopicId;
            context.SaveChanges();
            return RedirectToAction("Index", "Account");
        }
    }
}