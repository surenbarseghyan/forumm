﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Forum.Models;
using Microsoft.AspNetCore.Authorization;
using Forum.Data;
using Microsoft.EntityFrameworkCore;

namespace Forum.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext context;
        public HomeController(ApplicationDbContext context,ILogger<HomeController> logger)
        {
            this.context = context;
            _logger = logger;
        }

        
        public IActionResult Index()
        {
           List<Topic> topics = context.Topics.ToList();   
            return View(topics);
        }
        public IActionResult Posts(int id)
        {
            List<Post> Posts = context.Posts.Where(p => p.TopicId == id).Include(p=>p.User).ToList();
            return View(Posts);
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
