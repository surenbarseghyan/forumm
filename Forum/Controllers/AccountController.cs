﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.Data;
using Forum.Models;
using Forum.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Forum.Controllers
{
    public class AccountController : Controller
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<User> userManager;

        public AccountController(ApplicationDbContext context, UserManager<User> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }
        [Route("/Account/{id?}")]
        public  async Task<IActionResult> Index(string id)
        {
            var user = await userManager.GetUserAsync(User);
            User user_1=null;
            var posts = context.Posts.Where(p=>p.UserId == id).Include(p=>p.Topic).ToList();
            if (user.Id != id)
            {
                user_1= context.Users.Where(u => u.Id == id).SingleOrDefault();
            }
            ProfileViewModels models = new ProfileViewModels
            {
                Posts = posts,
                FirstName = user.FirstName,
                LastName = user.LastName,
                ProfilePicture = user.ProfilePicture
            };
            foreach (var post in models.Posts)
            {
                var summary = post.Description.Substring(0, 250);
                post.Description = summary;
            }
            if (user_1==null)
            {
                return View(models);
            }
            else
            {
                models.FirstName = user_1.FirstName;
                models.LastName = user_1.LastName;
                return View("OtherUserPosts", models);
            }
        }
    }
}